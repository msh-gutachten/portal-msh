const router = require('express').Router();
const User = require('../models/userModel');
const bcrypt = require('bcryptjs');
const role = require('../middleware/role');

router.get('/getall', async (req, res) => {
    res.send(await User.find().where({role: { $ne : "Mitarbeiter" }}));
})

router.post('/add', async (req, res) => {
    const salt = await bcrypt.genSalt(10);
    req.body.password = await bcrypt.hash(req.body.password, salt);

    try {
		const response = await User.create({
			email: req.body.email,
			password: req.body.password, 
            name: req.body.name
		})
        res.json(response);
	} catch (error) {
		if (error.code === 11000) {
			return res.json({ status: 'error', error: 'email already in use' })
		}
		throw error;
	}

    res.send({email: req.body.email, name: req.body.name});
});

// router.post('/update', async (req, res) => {
//     const { error } = User.validateUpdate(req.body);
//     if(error) return res.status(422).send(error.details[0].message);

//     const user = await User.findOne(req.body.email);
//     if(!user) return res.status(400).send('User with this email not exist');

//     if(req.body.password) {
//         const salt = await bcrypt.genSalt(10);
//         req.body.password = await bcrypt.hash(req.body.password, salt);
//     } else {
//         req.body.password = user.password;
//     }

    // await User.update(req.body);
    // res.send(User.getAll());
// })

router.post('/del', async (req, res) => {
    const user = await User.findOne({email: req.body.email});
    if(!user) return res.status(400).send('User with this email not exist');

    const result = await User.deleteOne({email: req.body.email})
    if(result.ok != 1) return res.status(404).send('failed');
    
    res.send(await User.find());
})

module.exports = router;

