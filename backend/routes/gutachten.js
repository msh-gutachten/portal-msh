const router = require('express').Router();
const role = require('../middleware/role');
const Gutachten = require('../models/gutachtenModel');

router.post('/getAll', role, async (req, res) => {
    res.json(await Gutachten.find());
});

router.post('/getByUser', async (req, res) => {
    res.send(await Gutachten.find({user_nr: req.user._id}));
});

router.post('/add', role, async (req, res) => {
    res.json(await Gutachten.create(req.body));
});

router.post('/update', role, async (req, res) => {
    await Gutachten.findByIdAndUpdate({_id: req.body._id}, req.body);
    res.json(await Gutachten.findOne({_id: req.body._id}));
});

router.post('/del', role, async (req, res) => {
    res.json(await Gutachten.deleteOne({_id: req.body.id}));
});

module.exports = router;