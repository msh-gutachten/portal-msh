const router = require('express').Router();
const bcrypt = require('bcryptjs');
const User = require('../models/userModel');
const jwt = require('jsonwebtoken');

router.post('/login', async (req, res) => {
	const user = await User.findOne({ email: req.body.email }).lean();
	if (!user) return res.status(404).send('Invalid user');

	const validPassword = await bcrypt.compare(req.body.password, user.password);
    if(!validPassword) return res.status(404).send('Invalid password');

    const token = jwt.sign({ _id: user._id, name: user.name, role: user.role }, process.env.ACCESS_TOKEN_SECRET);

    res.json({user: user, accessToken: token});
})

module.exports = router;