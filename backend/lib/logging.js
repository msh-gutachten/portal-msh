'use strict';
const winston = require('winston');
require('express-async-errors');

module.exports = () => {
    process.on('uncaughtException', (ex) => {
        winston.error(ex.message, ex);
        winston.log('error', ex.message, { message: ex });
        process.exit(1);
    });

    process.on('unhandledRejection', (ex) => {
        winston.error(ex.message, ex);
        winston.log('error', ex.message, { message: ex });
        process.exit(1);
    })

    process.on('disconnect', (ex) => {
        winston.info(ex.error, ex);
        process.exit(1);
    })

    winston.add(new winston.transports.File({
        filename: './logs/info.log'
    }));

    winston.add(new winston.transports.File({
        filename: './logs/error.log',
        level: 'error'
    }));
    
    winston.add(new winston.transports.Console({
        format: winston.format.simple(),
    }))
}