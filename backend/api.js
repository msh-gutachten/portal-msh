const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const winston = require('winston');

const authRoute = require('./routes/auth');
const userRoute = require('./routes/user');
const gutachtenRoute = require('./routes/gutachten');
const uploadRoute = require('./routes/upload');

const auth = require('./middleware/auth');
const role = require('./middleware/role');

const app = express();

require('./lib/logging')();

mongoose.connect('mongodb://188.68.33.153/msh', {
    user: process.env.DB_USER, 
    pass: process.env.DB_PW, 
    useNewUrlParser: true, 
    useUnifiedTopology: true, 
    useCreateIndex: true, 
});

mongoose.connection.on('connected', () => {  
    winston.info('Mongoose connection opened');
});

app.use(cors());
app.use(express.json());

app.use('/api/auth', authRoute);
app.use('/api/user',[auth, role], userRoute);
app.use('/api/gutachten', auth, gutachtenRoute);
app.use('/api/upload', auth, uploadRoute);



app.listen(process.env.API_PORT, () => winston.info('API Start: http://127.0.0.1:' + process.env.API_PORT));
