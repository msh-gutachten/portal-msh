const mongoose = require("mongoose");

const GutachtenSchema = new mongoose.Schema({
    gnr: { type: String, required: true, unique: true },
    kennzeichen: { type: String, required: true},
    user_nr: { type: String, required: true},
    unfalldatum: { type: Date, required: true},
    bemerkung: { type: String},
    reperatur: { type: Boolean, required: true},
    status: { type: Number, required: true},
    car: { type: String, required: true },
    kosten: { type: [Number], required: true },
}, { collection: "gutachten"});

module.exports = mongoose.model('Gutachten', GutachtenSchema);