module.exports = (req, res, next) => {
    if (req.user.role == "Normal") { 
        res.status(403).send('Permission Denied!: Not a Team member'); 
    } else {
        next();
    }
}