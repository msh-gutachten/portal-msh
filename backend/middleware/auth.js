'use strict';
const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
        const token = req.header('x-access-token');
        if(!token) return res.status(401).send('Access Denied!: No token provided');

        try {
            req.user = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
            next();            
        } catch (error) {
            res.status(400).send('Invalid token');
        }
}