﻿import { Routes, RouterModule } from '@angular/router';
import { PublicLayoutComponent } from './_layout/public-layout/public-layout.component';
import { PrivateLayoutComponent } from './_layout/private-layout/private-layout.component';
import { AuthGuard } from './_guards/auth.guard';
import { MitarbeiterGuard } from './_guards/mitarbeiter.guard';
import { LoginComponent } from './login';
import { GutachtenComponent } from './content/gutachten/gutachten.component';
import { EinstellungenComponent } from './content/einstellungen/einstellungen.component';
import { KundenComponent } from './content/kunden/kunden.component';

const appRoutes: Routes = [
  // Public layout
  {
    path: '',
    component: PublicLayoutComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: '', component: LoginComponent }
    ]
  },
  // Private layout
  {
    path: '',
    component: PrivateLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      // { path: 'login', component: LoginComponent },
      { path: 'gutachten', component: GutachtenComponent },
      { path: 'einstellungen', component: EinstellungenComponent },
      { path: 'kunden', component: KundenComponent, canActivate: [MitarbeiterGuard] },
    ],
  },
  // otherwise redirect to home
  { path: '**', redirectTo: 'gutachten' }
];

export const routing = RouterModule.forRoot(appRoutes, { scrollOffset: [0, 0], scrollPositionRestoration: 'top'});
