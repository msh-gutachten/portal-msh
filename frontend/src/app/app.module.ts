﻿import { NgModule } from '@angular/core';
import { BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbModule, NgbCarouselConfig, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

import { AngularFireAuthModule } from 'angularfire2/auth';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from '../environments/environment';
import { AuthGuard } from './_guards/auth.guard';
import { MitarbeiterGuard } from './_guards/mitarbeiter.guard';
import { AlertComponent } from './_directives/alert.component';
import { AlertService } from './_services/alert.service';
import { AuthService } from './_services/auth.service';

// Routing
import { routing } from './app.routing';

// Components
import { AppComponent } from './app.component';

import { SettingsModule } from './_layout/settings/settings.module';
import { ThemeSettingsConfig } from './_layout/settings/theme-settings.config';
import { MenuSettingsConfig } from './_layout/settings/menu-settings.config';

import { HeaderComponent } from './_layout/header/header.component';
import { VerticalComponent as HeaderVerticalComponent } from './_layout/header/vertical/vertical.component';
import { HorizontalComponent as HeaderHorizontalComponent } from './_layout/header/horizontal/horizontal.component';
import { FullLayoutNavbarComponent } from './_layout/header/full-layout-navbar/full-layout-navbar.component';

import { FooterComponent } from './_layout/footer/footer.component';
import { NavigationComponent as AppNavigationComponent } from './_layout/navigation/navigation.component';

import { PublicLayoutComponent } from './_layout/public-layout/public-layout.component';
import { PrivateLayoutComponent } from './_layout/private-layout/private-layout.component';

import { LoginComponent } from './login';
import { NavbarService } from './_services/navbar.service';
import { VerticalnavComponent } from './_layout/navigation/verticalnav/verticalnav.component';
import { HorizontalnavComponent } from './_layout/navigation/horizontalnav/horizontalnav.component';

// perfect scroll bar
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
// spinner
import { NgxSpinnerModule } from 'ngx-spinner';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';

import { DeviceDetectorModule } from 'ngx-device-detector';
import { RouterModule } from '@angular/router';
import { PartialsModule } from './content/partials/partials.module';
import { BreadcrumbModule } from './_layout/breadcrumb/breadcrumb.module';
// import { DataApiService } from './_services/data.api';
import { BlockTemplateComponent } from './_layout/blockui/block-template.component';
import { BlockUIModule } from 'ng-block-ui';
import { MatchHeightModule } from './content/partials/general/match-height/match-height.module';
import { FullLayoutComponent } from './_layout/full-layout/full-layout.component';;
import { GutachtenComponent } from './content/gutachten/gutachten.component';
import { EinstellungenComponent } from './content/einstellungen/einstellungen.component';
import { KundenComponent } from './content/kunden/kunden.component';
import { TokenStorageService } from './_services/token-storage.service';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};

import { ChartsModule } from 'ng2-charts';
import { GutachtenService } from './_services/gutachten.service';;
import { GutachtenCardComponent } from './_layout/gutachten-card/gutachten-card.component';
import { BarchartCardComponent } from './_layout/barchart-card/barchart-card.component';
import { UserService } from './_services/user.service';
import { UploadService } from './_services/upload.service';

// Material Design
import { MatTabsModule } from '@angular/material/tabs';
import { StatusPipe } from './_pipes/status.pipe';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgSelectModule } from '@ng-select/ng-select';

import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { TreeviewModule } from 'ngx-treeview';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { UiSwitchModule } from 'ngx-ui-switch';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';


@NgModule({
    imports: [
        NgxSkeletonLoaderModule,
        UiSwitchModule,
        NgxDropzoneModule,
        TreeviewModule.forRoot(),
        NgxDatatableModule,
        MatTabsModule,
        ChartsModule,
        BrowserModule,
        PartialsModule,
        ReactiveFormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatchHeightModule,
        BreadcrumbModule,
        NgbModule,
        NgSelectModule,
        FormsModule,
        routing,
        // Settings modules
        SettingsModule.forRoot(ThemeSettingsConfig, MenuSettingsConfig),
        PerfectScrollbarModule,
        NgxSpinnerModule,
        DeviceDetectorModule.forRoot(),
        LoadingBarRouterModule,
        BlockUIModule.forRoot({
            template: BlockTemplateComponent
        })
    ],
    declarations: [
        AppComponent,
        PublicLayoutComponent,
        PrivateLayoutComponent,
        HeaderComponent,
        FullLayoutNavbarComponent,
        HeaderHorizontalComponent,
        HeaderVerticalComponent,
        FooterComponent,
        AppNavigationComponent,
        AlertComponent,
        LoginComponent,
        VerticalnavComponent,
        HorizontalnavComponent,
        BlockTemplateComponent,
        FullLayoutComponent,
        GutachtenComponent,
        EinstellungenComponent,
        KundenComponent,
        GutachtenCardComponent,
        BarchartCardComponent,
        StatusPipe,
    ],
    providers: [
        AuthGuard,
        MitarbeiterGuard,
        // AuthService,
        TokenStorageService,
        AlertService,
        GutachtenService,
        UserService,
        UploadService,
        NavbarService,
        AuthService,
        // {
        //     provide: HAMMER_GESTURE_CONFIG,
        //     useClass: HammerGestureConfig
        // },
        NgbCarouselConfig,
        NgbModalConfig,
        authInterceptorProviders
    ],
    entryComponents: [
        BlockTemplateComponent
    ],
    bootstrap: [AppComponent],
    exports: [RouterModule]
})

export class AppModule {

}
