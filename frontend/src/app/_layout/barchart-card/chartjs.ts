///////////////////// Start Barchart ///////////////
export const barChartOptions: any = {
  responsive: true,
  scaleShowVerticalLines: true,
  maintainAspectRatio: false,
  scales: {
    dataset: [{
          categoryPercentage: 0.5
        }]
}
};
export const barChartLabels: string[] = ['Reperaturkosten', 'Wertminderung', 'SV-Kosten', 'Fremdkosten'];
export const barChartType = 'bar';
export const barChartLegend = false;
export const barChartData: any[] = [
  { data: [65, 59, 80, 81] },
];
export const barChartColors: Array<any> = [
  {
    backgroundColor: '#28d094',
    borderColor: '#28d094',
    pointBackgroundColor: '#28d094',
    pointBorderColor: '#28d094',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: '#28d094',
    },
];
///////////////////// End barchart////////////////



