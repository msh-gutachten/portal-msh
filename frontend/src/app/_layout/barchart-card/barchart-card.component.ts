import { Component, Input, OnInit } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

import * as chartsData from './chartjs';

@Component({
  selector: 'app-barchart-card',
  templateUrl: './barchart-card.component.html',
  styleUrls: ['./barchart-card.component.css']
})
export class BarchartCardComponent implements OnInit {
  @BlockUI('barCharts') blockUIBarCharts: NgBlockUI;
  @Input() barChartData;

  public barChartOptions = chartsData.barChartOptions;
  public barChartLabels = chartsData.barChartLabels;
  public barChartType = chartsData.barChartType;
  public barChartLegend = chartsData.barChartLegend;
  public barChartColors = chartsData.barChartColors;
  constructor() { }

  ngOnInit(): void {
  }

}
