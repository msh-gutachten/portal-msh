import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GutachtenCardComponent } from './gutachten-card.component';

describe('GutachtenCardComponent', () => {
  let component: GutachtenCardComponent;
  let fixture: ComponentFixture<GutachtenCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GutachtenCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GutachtenCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
