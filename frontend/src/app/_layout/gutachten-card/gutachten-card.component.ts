import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TreeviewItem, TreeviewConfig } from 'ngx-treeview';
import { AppConstants } from 'src/app/_helpers/app.constants';
import { AuthService } from 'src/app/_services/auth.service';
import { GutachtenService } from 'src/app/_services/gutachten.service';
import { UploadService } from 'src/app/_services/upload.service';

@Component({
  selector: 'app-gutachten-card',
  templateUrl: './gutachten-card.component.html',
  styleUrls: ['./gutachten-card.component.css']
})
export class GutachtenCardComponent implements OnInit {

  basicConfig = TreeviewConfig.create({
    hasAllCheckBox: false,
    hasFilter: false,
    hasCollapseExpand: true,
    maxHeight: 400
  });
  docs;

  @Input() data;
  @Output() deleteRequest = new EventEmitter<any>();
  @Output() updateRequest = new EventEmitter<any>();

  @ViewChild('mCard', { static: true }) elCard: ElementRef;
  @ViewChild('mCardHeader', { static: true }) elHeader: ElementRef;
  @ViewChild('mCardHeaderTitle', { static: true }) elHeaderTitle: ElementRef;
  @ViewChild('mCardContent', { static: true }) elContent: ElementRef;
  @ViewChild('mCardBody', { static: true }) elBody: ElementRef;
  @ViewChild('mCardFooter', { static: true }) elFooter: ElementRef;
  @ViewChild('mCardHeaderTools', { static: true }) elHeaderTools: ElementRef;


  kosten: any[];

  constructor(
    private authService: AuthService,
    private gutachtenService: GutachtenService,
    private modalService: NgbModal,
    private uploadService: UploadService
  ) { 
  }

  ngOnInit(): void {
  }

  update() {
    this.updateRequest.emit();
  }

  openDeleteModal(DeleteModal) {
    this.modalService.open(DeleteModal, { windowClass: 'animated fadeInDown' });
  }

  removeGutachten() {
    this.toggleMobileMenu();

    this.gutachtenService.remove(this.data._id).subscribe(() => {
      this.deleteRequest.emit();
    });
  }

  toggleCollpase(event) {
    let target = event.target || event.srcElement || event.currentTarget;
    if (this.elContent.nativeElement.classList.contains('show')) {
      this.elContent.nativeElement.classList.add('collapse');
      this.elContent.nativeElement.classList.remove('show');
      if (!target.classList.contains('ft-plus') && !target.classList.contains('ft-minus')) {
        target = event.target.querySelector('i');
      }
      target.classList.remove('ft-minus');
      target.classList.add('ft-plus');
    } else {
      this.elContent.nativeElement.classList.add('show');
      if (!target.classList.contains('ft-plus') && !target.classList.contains('ft-minus')) {
        target = event.target.querySelector('i');
      }
      this.elContent.nativeElement.classList.remove('collapse');
      target.classList.remove('ft-plus');
      target.classList.add('ft-minus');
      this.loadDocs();
      this.kosten = this.data.kosten;
    }
    this.toggleMobileMenu();
  }

  loadDocs() {
    this.uploadService.getById(this.data._id).subscribe(data => {
      this.docs = this.groupBy(data, cat => cat.category);
    })
  }

  toggleMobileMenu() {
    if (this.elHeaderTools.nativeElement.classList.contains('visible')) {
      this.elHeaderTools.nativeElement.classList.remove('visible');
    } else {
      this.elHeaderTools.nativeElement.classList.add('visible');
    }
    // fire resize event for graphs
    setTimeout(() => { AppConstants.fireRefreshEventOnWindow(); }, 300);
  }

  groupBy(list, keyGetter) {
    const map = new Map();
    list.forEach((item) => {
      const key = keyGetter(item);
      const collection = map.get(key);
      if (!collection) {
        map.set(key, [item]);
      } else {
        collection.push(item);
      }
    });
    return map;
  }

  public haveDocs(): boolean {
    if (this.docs == null) {
      return false;
    } else {
      if (this.docs == []) {
        return false;
      } else {
        return true;
      }
    }
  }
}