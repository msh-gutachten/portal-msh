// Default menu settings configurations

export interface MenuItem {
  title: string;
  icon: string;
  page: string;
  isExternalLink?: boolean;
  issupportExternalLink?: boolean;
  badge: { type: string, value: string };
  submenu: {
    items: Partial<MenuItem>[];
  };
  section: string;
  role: boolean;
}

export interface MenuConfig {
  vertical_menu: {
    items: Partial<MenuItem>[]
  };
}

export const MenuSettingsConfig: MenuConfig = {
  vertical_menu: {
    items: [
      {
        title: 'Gutachten',
        icon: 'la-file-text',
        page: '/gutachten',
        role: false,
      }, {
        title: 'Kunden',
        icon: 'la-user',
        page: '/kunden',
        role: true,
      }, 
    ]
  }

};





