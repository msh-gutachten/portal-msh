export enum Status {
    'In Bearbeitung' = 1, 
    Reguliert = 2, 
    Teilreguliert = 3, 
    Unvollständig = 4, 
    Abgelehnt = 5, 
    Abgeschlossen = 6
}
