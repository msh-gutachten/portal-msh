export enum Cars {
    COMBI = "/assets/images/cars/COMBI.png",
    COMPACT_CAR_3DOORS = "/assets/images/cars/COMPACT_CAR_3DOORS.png",
    COMPACT_CAR_5DOORS = "/assets/images/cars/COMPACT_CAR_5DOORS.png",
    COUPE = "/assets/images/cars/COUPE.png",
    CUBICLE = "/assets/images/cars/CUBICLE.png",
    DELIVERY_VAN = "/assets/images/cars/DELIVERY_VAN.png",
    ENDURO = "/assets/images/cars/ENDURO.png",
    LIMOUSINE = "/assets/images/cars/LIMOUSINE.png",
    MINIBUS = "/assets/images/cars/MINIBUS.png",
    SUV = "/assets/images/cars/SUV.png",
    VAN = "/assets/images/cars/VAN.png"
}
 