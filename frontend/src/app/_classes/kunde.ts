export class Kunde {
    constructor(
        public id: number,
        public name: string,
        public email: string,
        public phone: string,
        public image: any,
        public isFavorite: boolean,
        public isActive: string
      ) { }
}
