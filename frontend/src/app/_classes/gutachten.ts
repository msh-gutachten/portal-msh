import { Status } from "../_enums/status.enum";

export class Gutachten {
    id?: string;
    gnr: string;
    kennzeichen: string;
    reperatur: boolean;
    status: Status;
    bemerkung: string;
    unfalldatum: Date;

    constructor(gnr?: string, kennzeichen?: string, reperatur?: boolean, status?: Status, bemerkung?: string, unfalldatum?: Date){
        this.gnr = gnr;
        this.kennzeichen = kennzeichen;
        this.reperatur = reperatur;
        this.status = status;
        this.bemerkung = bemerkung;
        this.unfalldatum = unfalldatum;
    }
}
