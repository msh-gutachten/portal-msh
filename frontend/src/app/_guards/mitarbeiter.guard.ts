import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from '../_services/auth.service';

@Injectable()
export class MitarbeiterGuard implements CanActivate {
  constructor(private authService: AuthService){}

  canActivate(){
    if(this.authService.getRole == "Mitarbeiter") return true;
    return false;
  }
  
}
