﻿import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from '../_services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private authService: AuthService) { }

    canActivate() {
        if (this.authService.isLogedIn) {
            return true;
        } else {
            this.authService.logout();
            return false;
        }
    }
}
