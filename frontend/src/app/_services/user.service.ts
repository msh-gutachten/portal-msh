import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const KUNDEN_API = environment.backend.server + environment.backend.userLink;

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  insert(pKunde: any): Observable<any> {
    return this.http.post(KUNDEN_API + '/add', pKunde, httpOptions);
  }

  update(pKunde: any): Observable<any> {
    return this.http.post(KUNDEN_API + '/update', pKunde, httpOptions);
  }

  getAll(): Observable<any> {
    return this.http.get(KUNDEN_API + '/getall', httpOptions);
  }

  remUser(pKunde: any): Observable<any> {
    return this.http.post(KUNDEN_API + '/del', pKunde, httpOptions);
  }
}
