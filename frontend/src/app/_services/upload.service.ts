import { HttpClient, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const UPLOAD_API = environment.backend.server + environment.backend.uploadLink;

@Injectable({
    providedIn: 'root'
})
export class UploadService {
    constructor(
        private http: HttpClient,
    ) { }

    uploadFile(pFiles, pGID): Observable<any> {

        let formData = new FormData();

        formData.append('gid', pGID);
        
        pFiles.forEach(file => {
            console.log(file);
            
            formData.append('type', file.type);
            formData.append('file', file.file, file.name);
        });

        console.log("files: ", formData.getAll);
        

        return this.http.post(UPLOAD_API + '/add', formData);
    }


    downloadFile(pFileId): Observable<Blob> {
        return this.http.post(UPLOAD_API + '/download', pFileId, {
            responseType: 'blob'
        });
    }


    getById(pId): Observable<any> {
        return this.http.post(UPLOAD_API + '/getById', {gid: pId});
    }


    

}