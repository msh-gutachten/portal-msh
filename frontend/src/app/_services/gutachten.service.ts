import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Gutachten } from '../_classes/gutachten';

const GUTACHTEN_API = environment.backend.server + environment.backend.gutachtenLink;

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class GutachtenService {

  constructor(private http: HttpClient) { }

  insert(pGutachten): Observable<any> {
    return this.http.post(GUTACHTEN_API + '/add', pGutachten, httpOptions);
  }

  getAll(): Observable<any> {
    return this.http.post(GUTACHTEN_API + '/getAll', httpOptions);
  }

  getByUser(): Observable<any> {
    return this.http.post(GUTACHTEN_API + '/getByUser', httpOptions);
  }

  update(pGutachten): Observable<any> {
    return this.http.post(GUTACHTEN_API + '/update', pGutachten, httpOptions);
  }

  remove(pId): Observable<any> {
    return this.http.post(GUTACHTEN_API + '/del', {id: pId}, httpOptions);
  }

}
