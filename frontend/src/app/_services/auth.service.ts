import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TokenStorageService } from './token-storage.service';
import { Router } from '@angular/router';

const AUTH_API = environment.backend.server + environment.backend.authLink;

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class AuthService {
  constructor(
    private http: HttpClient, 
    private tokenService: TokenStorageService,
    private router: Router,
  ) { }

  login(email: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + '/login', {
      email,
      password
    }, httpOptions);
  }

  register(email: string, password: string, vorname: string, name: string): Observable<any> {
    return this.http.post(AUTH_API + '/register', {
      email,
      password,
      vorname,
      name
    }, httpOptions);
  }

  logout(){
    window.localStorage.clear();
    this.router.navigate(['/login']);
  }

  get isLogedIn(): boolean{
    if(this.getUser) return true;
    return false;
  }

  get getUser(): any {
    return this.tokenService.getUser();
  }

  get getRole(): string {
    return this.getUser.role;
  }

  get isMitarbeiter(): boolean {
    return this.getRole == 'Mitarbeiter';
  }
}
