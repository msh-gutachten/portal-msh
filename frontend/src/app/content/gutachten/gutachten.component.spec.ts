import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GutachtenComponent } from './gutachten.component';

describe('GutachtenComponent', () => {
  let component: GutachtenComponent;
  let fixture: ComponentFixture<GutachtenComponent>;

  beforeEach(waitForAsync (() => {
    TestBed.configureTestingModule({
      declarations: [ GutachtenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GutachtenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
