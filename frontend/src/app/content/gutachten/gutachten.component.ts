import { Component, OnInit } from '@angular/core';
import { Status } from 'src/app/_enums/status.enum';
import { AuthService } from 'src/app/_services/auth.service';
import { Gutachten } from '../../_classes/gutachten';
import { GutachtenService } from '../../_services/gutachten.service';
import { Role } from 'src/app/_enums/role.enum';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from 'src/app/_services/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UploadService } from 'src/app/_services/upload.service';


@Component({
  selector: 'app-gutachten',
  templateUrl: './gutachten.component.html',
  styleUrls: ['./gutachten.component.css']
})
export class GutachtenComponent implements OnInit {
  GutachtenForm: FormGroup;
  gutachten?: Gutachten[] = [];
  kunden?= [];
  selectedUser;
  allStatus = [
    {
      name: 'In Bearbeitung',
      val: 1
    },
    {
      name: 'Reguliert',
      val: 2
    },
    {
      name: 'Teilreguliert',
      val: 3
    },
    {
      name: 'Unvollständig',
      val: 4
    },
    {
      name: 'Abgelehnt',
      val: 5
    },
    {
      name: 'Abgeschlossen',
      val: 6
    },
  ];
  allCars = [
    {
      name: 'Kombi',
      val: 'assets/images/cars/COMBI.png'
    },
    {
      name: 'Kompaktauto 3 Türe',
      val: 'assets/images/cars/COMPACT_CAR_3DOORS.png'
    },
    {
      name: 'Kompaktauto 5 Türe',
      val: 'assets/images/cars/COMPACT_CAR_5DOORS.png'
    },
    {
      name: 'Coupé',
      val: 'assets/images/cars/COUPE.png'
    },
    {
      name: 'Cubicle',
      val: 'assets/images/cars/CUBICLE.png'
    },
    {
      name: 'Lieferwagen',
      val: 'assets/images/cars/DELIVERY_VAN.png'
    },
    {
      name: 'Motorrad',
      val: 'assets/images/cars/ENDURO.png'
    },
    {
      name: 'Limousine',
      val: 'assets/images/cars/LIMOUSINE.png'
    },
    {
      name: 'Minibus',
      val: 'assets/images/cars/MINIBUS.png'
    },
    {
      name: 'SUV',
      val: 'assets/images/cars/SUV.png'
    },
    {
      name: 'Van',
      val: 'assets/images/cars/VAN.png'
    },
  ];
  selectedFiles = [];
  kosten: any[];


  constructor(
    private gutachtenService: GutachtenService,
    private userService: UserService,
    private authService: AuthService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private uploadService: UploadService,
  ) { }
  public breadcrumb: any;

  ngOnInit(): void {
    this.breadcrumb = {
      'mainlabel': 'Gutachten',
      'links': [
        {
          'name': this.authService.isMitarbeiter ? 'Alle Gutachten' : 'Meine Gutachten',
          'isLink': false
        }
      ],
    };

    this.loadGutachten();

    if (this.authService.getRole == 'Mitarbeiter') {
      this.loadUser();
    }
  }

  private loadGutachten() {
    if (this.gutachten != []) {
      return;
    }

    if (this.authService.getRole == Role.Mitarbeiter.toString()) {
      this.gutachtenService.getAll().subscribe(data => {
        if (data) {
          this.gutachten = data;
        }
      });
    } else {
      this.gutachtenService.getByUser().subscribe(data => {
        if (data) {
          this.gutachten = data;
        }
      });
    }
  }

  private async loadUser() {
    this.userService.getAll().subscribe(data => {
      data.forEach(user => {
        this.kunden.push(new Object({ name: user.name, id: user._id }));
      });
    });
  }

  openInsertModal(InsertModal, ErrorModal) {
    // if (this.kunden.length == 0) {
    //   this.modalService.open(ErrorModal, { windowClass: 'animated fadeInDown' });
    //   return;
    // }
    this.kosten = [0, 0, 0, 0];

    this.GutachtenForm = this.formBuilder.group({
      gnr: [null, Validators.required],
      kennzeichen: [null, Validators.required],
      user_nr: [null, Validators.required],
      unfalldatum: [new Date().toLocaleDateString('en-CA'), Validators.required],
      bemerkung: [null],
      reperatur: [false],
      status: [Status['In Bearbeitung']],
      car: this.allCars[0].val, 
      kosten: [this.kosten]
    })

    this.selectedFiles = [];

    this.modalService.open(InsertModal, { windowClass: 'animated fadeInDown', size: 'lg' });
  }

  onFilesAdded(event) {
    const files = event.target.files;

    for (let index = 0; index < files.length; index++) {
      const file = files[index];
      this.selectedFiles.push({
        name: file.name,
        type: "",
        file: file
      });
    }
  }

  removeFile(index) {
    this.selectedFiles.splice(index, 1);
  }

  checkStatus(form: FormGroup ,element){    
    if(form.get(element).value == null){
      return '';
    } else {
      return form.get(element).valid? 'border-success': 'border-danger';
    }
  }

  gutachtenAnlegen() {
    this.gutachtenService.insert(this.GutachtenForm.value).subscribe(data => {
      // this.gutachten.push(data);
      this.saveDocs(data._id);
    })
  }

  openUpdateModal(UpdateModal, pGutachten) {
    this.kosten = pGutachten.kosten;

    this.GutachtenForm = this.formBuilder.group({
      _id: [pGutachten._id],
      gnr: [pGutachten.gnr, Validators.required],
      kennzeichen: [pGutachten.kennzeichen, Validators.required],
      user_nr: [pGutachten.user_nr, Validators.required],
      unfalldatum: [new Date().toLocaleDateString('en-CA'), Validators.required],
      bemerkung: [pGutachten.bemerkung],
      reperatur: [pGutachten.reperatur],
      status: [pGutachten.status],
      car: [pGutachten.car], 
      kosten: [this.kosten]
    })

    this.selectedFiles = [];

    this.modalService.open(UpdateModal, { windowClass: 'animated fadeInDown', size: 'lg' });
  }

  gutachtenUpdaten() {
    this.gutachtenService.update(this.GutachtenForm.value).subscribe(data => {

      this.loadGutachten();

      // this.updateDocs(data._id);
    })
  }

  private saveDocs(pGID) {
    if (this.selectedFiles.length > 0) {
      this.uploadService.uploadFile(this.selectedFiles, pGID).subscribe(() => { },
        err => console.log('HTTP Error', err),
        () => console.log('upload completed.'),
      );
    }
  }

  // private updateDocs(pCID) {

  // }

  checkGutachten() {
    var anzahl = 0; 

    if(this.gutachten) {
      anzahl += this.gutachten.length;
    }

    return anzahl;
  } 
}
