import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/_services/auth.service';
import { UserService } from 'src/app/_services/user.service';


@Component({
  selector: 'app-kunden',
  templateUrl: './kunden.component.html',
  styleUrls: ['./kunden.component.css']
})
export class KundenComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private modalService: NgbModal,
    private userService: UserService,
    public formBuilder: FormBuilder,
  ) { }

  public breadcrumb: any;

  kunden = [];

  insertUserForm: FormGroup;
  updateUserForm: FormGroup;

  deleteUser;

  ngOnInit(): void {
    this.breadcrumb = {
      'mainlabel': 'Kunden',
      'links': [
        {
          'name': 'Alle Kunden',
          'isLink': false
        }
      ],
    };

    this.loadKunden();
  }


  checkStatus(form: FormGroup ,element){    
    if(form.get(element).value == null){
      return '';
    } else {
      return form.get(element).valid? 'border-success': 'border-danger';
    }
  }

  openInsertModal(InputModal) {
    this.insertUserForm = this.formBuilder.group({
      name: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength(8), Validators.maxLength(100)]]
    });

    this.modalService.open(InputModal, { windowClass: 'animated fadeInDown' });
  }

  openUpdateModal(UpdateModal, pUser) {
    this.updateUserForm = this.formBuilder.group({
      name: [pUser.name, [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
      email: [pUser.email, [Validators.required, Validators.email]],
      password: ['', [Validators.minLength(8), Validators.maxLength(100)]]
    })

    this.modalService.open(UpdateModal, { windowClass: 'animated fadeInDown' });
  }

  openDeleteModal(DeleteModal, pUser) {
    this.modalService.open(DeleteModal, { windowClass: 'animated fadeInDown' });
    this.deleteUser = pUser;
  }

  loadKunden() {
    this.userService.getAll().subscribe(data => {
      this.kunden = data;
    });
  }

  kundenAnlegen() {
    if (this.insertUserForm.valid) {
      this.userService.insert(this.insertUserForm.value).subscribe(data => {
        this.kunden.push(data);
      })
    }
  }

  editUser() {
    let user;
    if (this.updateUserForm.value.password != '') {
      user = {
        password: this.updateUserForm.value.password,
        name: this.updateUserForm.value.name,
        email: this.updateUserForm.value.email,
      }
    } else {
      user = {
        name: this.updateUserForm.value.name,
        email: this.updateUserForm.value.email,
      };
    }

    this.userService.update(user).subscribe(data => {
      this.kunden = data;
    })

  }

  delUser() {
    this.userService.remUser(this.deleteUser).subscribe(data => {
      this.kunden = data;
    });
  }

}
