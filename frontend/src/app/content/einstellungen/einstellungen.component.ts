import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { UserService } from 'src/app/_services/user.service';
import { map } from 'rxjs/operators';
import { User } from 'src/app/_classes/user';
import { AuthService } from 'src/app/_services/auth.service';


@Component({
  selector: 'app-einstellungen',
  templateUrl: './einstellungen.component.html',
  styleUrls: ['./einstellungen.component.css']
})
export class EinstellungenComponent implements OnInit {

  @BlockUI('personalInfo') blockUIProjectInfo: NgBlockUI;

  personalInfo: FormGroup;
  changePassword: FormGroup;


  constructor(private formBuilder: FormBuilder, private authService: AuthService) { }
  public breadcrumb: any;
  public userData: any;


  ngOnInit(): void {
    this.breadcrumb = {
      'mainlabel': 'Einstellungen',
    };
    
    this.userData = this.authService.getUser;

    this.personalInfo = this.formBuilder.group({
      name: { value: this.userData.name || '', disabled: true },
      email: { value: this.userData.email || '', disabled: true },
      phone: [this.userData.phone || '', Validators.required],
    });

    this.changePassword = this.formBuilder.group({
      currentPasswort: ['', Validators.required, Validators.minLength(6)],
      newPasswort: ['', Validators.required, Validators.minLength(6)],
      confirmPasswort: ['', Validators.required, Validators.minLength(6)],
    });
  }

}
