// WARNING - Values under "firebase" and value of "googleApiKey" needs to be replaced from your own accounts
// If left as is, it firbase and google map related functionality will not work on LIVE instance.
export const environment = {
  backend: {
    server: "http://localhost:3000", 
    authLink: "/api/auth", 
    gutachtenLink: "/api/gutachten",
    userLink: "/api/user",
    uploadLink: "/api/upload"
  },
  production: true,
  googleApiKey: 'AIzaSyAIIYOxA7qeetFz6TuR1Qewc0Rrjhzx7ZU'
};
